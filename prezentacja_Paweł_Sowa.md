---
author: Paweł Sowa
title: Szachy
subtitle: Krótka prezentacja o historii i zasadach
date: 31.10.2021r.
theme: Warsaw
output: beamer_presentation
header-includes: \usepackage{xcolor}
    \usepackage{listings}
    \usepackage[utf8]{inputenc}
    \usepackage{polski}
---

## 1. Początki szachów

Przyjmuje się, że szachy powstały w północno-zachodnich Indiach w
okresie Imperium Guptów (ok. 280–550). Wówczas grano w
czaturangę, która jest pierwszą znaną grą, która później
wyewoluowała do szachów współczesnych.

W dosłownym tłumaczeniu czaturanga oznacza

-   **„czterostronny”**
-   **„podzielony na cztery części”**

![Plansza czaturangi](./img/1.png "Plansza czaturangi"){ height=30% width=30%}

## 2. Szatrandż

Zapewne poprzez kupców poruszających się Jedwabnym Szlakiem
czaturanga dotarła do Persji. Tam gra uległa modyfikacji, przez co
zmieniła się w szatrandż. Pojawiły się figury, których zakres ruchów i
ułożenie startowe są już bliskie współczesnym szachom. Figurami
były:

1. al-szach (król),
2. al-firzan („wezyr” lub „mędrzec”, dziś hetman lub królowa),
3. al-roch („zamek”, dziś wieża),
4. al-fil („słoń”, dziś goniec),
5. al-faras („jeździec” dziś skoczek),
6. al-beizaq („piechur” dziś pion).

Ciekawostką jest, że znane nam dziś określenie szach-mat wywodzi się od perskiego _shāh māt_, co oznacza _„król jest bezradny”_.

## 3. Szachy w Europie

Od końca XV wieku szachy zaczęły zyskiwać w Europie coraz
większą popularność. Szczególnie zacięta rywalizacja toczyła się
między hiszpańskimi, a włoskimi mistrzami. Jednym z najbardziej
znanych miłośników szachów był król Hiszpanii, Filip II. Na jego
dworze służył Ruy Lopez de Segura, który także uwielbiał grać w
szachy.

![XVI-wieczni szachiści](./img/2.jpeg "XVI-wieczni szachiści"){ height=50% width=50%}

## 4. Rozwój szachów na dworach królewskich

Szachy nie były zajęciem tylko dla graczy wywodzących się z
akademii szachowych. Stały się popularną rozrywką na wielu
europejskich dworach.
Wśród polskich królów zapewne grywał w nie **Kazimierz Wielki**, ze
względu na swoje kontakty z Andegawenami, którzy lubili tę grę.

Fanem szachów był także **Kazimierz Jagiellończyk** oraz
**Zygmunt Stary**. Ostatni z wymienionych władców był chyba
najbardziej zawziętym graczem spośród polskich królów. Podczas
pobytu u swojego brata na węgierskim dworze przegrywał niemałe
sumy przy szachownicy. Zachował się rachunek króla za **naprawę
szachownicy**, co oznacza, że musiała być przez niego mocno
używana.

## 5. Początki organizacji szachowych stowarzyszeń

Pierwszym już oficjalnym mistrzem świata został Wilhelm Steinitz,
który pokonał w pierwszym pojedynku o ten tytuł szachistę polsko-
niemieckiego pochodzenia. Johannesa Zukertorta. Steinitz wygał w
stosunku 12½ - 7½.

W 1924 roku powstała Międzynarodowa Federacja Szachowa (FIDE).
Z inicjatywy francuskich szachistów udało się porozumieć piętnastu
europejskim związkom szachowym, celem stworzenia wspólnej
organizacji. Wśród założycieli FIDE była także Polska.

Od 1924 roku rozgrywana jest olimpiada szachowa. Polska
reprezentacja w latach 20-tych i 30-tych wypadała w niej
rewelacyjnie, zajmując czołowe miejsca. Największym sukcesem było
zwycięstwo w 1930 roku. Polską reprezentację do tego triumfu
poprowadził wybitny mistrz Akiba Rubinstein.

## 6. Mecz stulecia

Za szachowy mecz stulecia uznawany jest pojedynek rozegrany
między Amerykaninem, Bobby Fischerem, a radzieckim
arcymistrzem, Borisem Spasskim. Meczy odbył się w 1972 roku w
Rejkaviku.
Fisher stawiał organizatorom coraz to nowe wymagania. Pierwszą
partię Fischer łatwo przegrał, a do drugiej w ogóle się nie stawił,
oddając ją walkowerem. Po długich negocjacjach przystąpił do
trzeciej partii, którą wygrał. Ostatecznie pokonał Spasskiego w
stosunku 12½ - 8½.

![Fisher i Spasski](./img/3.jpg "Fisher i Spasski"){ height=50% width=50%}

## 7. Deep Blue

Rozwój komputerów przyczynił się także do rozwoju szachowych
programów komputerowych. Toteż w 1996 roku postanowiono
zorganizować pojedynek Kasparowa z programem komputerowym
przygotowanym przez IBM. Pojedynek pomiędzy komputerem Deep
Blue, a człowiekiem wygrał Kasparow stosunkiem 4 - 2.

Do kontrowersyjnego rewanżu doszło w 1997 roku. Wówczas
komputer Deeper Blue pokonał Kasparowa w stosunku 3½ - 2½. Tym
samym IBM mógł szczycić się stworzeniem komputera zdolnego
pokonać arcymistrza.

## 8. Zasady gry

Na początku gry szachownica jest ułożona w ten sposób, że
zawodnik ma białe (lub jasne) pole na najbliższej mu linii po prawej
stronie.

Początkowe ustawienie figur jest zawsze takie samo. Piony stoją na
drugiej linii. Miejscem wież są rogi szachownicy. Obok nich stoją
skoczki, a dalej gońce. Na centralnych polach stoi hetman (który
zawsze zajmuje pole własnego koloru) oraz król.

![Ustawienie figur na szachownicy](./img/4.png "Ustawienie figur na szachownicy"){ height=40% width=40%}

## 9. Król

Król jest najważniejszą figurą, ale jedną z najsłabszych. Król może
poruszać się tylko o jedno pole w dowolnym kierunku - w górę, w dół,
na boki, i po skosie. Król nigdy nie może wejść na pole szachowane
(na którym mógłby zostać zbity). Kiedy król jest atakowany przez inną
figurę, nazywamy to "szach".

![Król](./img/5.png "Król"){ height=40% width=40%}

## 10. Hetman - 9 pkt

Hetman jest najsilniejszą figurą. Może iść wzdłuż jednego z
kierunków - naprzód, w tył, w boki lub po skosie - tak daleko, jak jest
to możliwe, pod warunkiem że nie przechodzi przez figury tego
samego koloru, co on sam. I, jak ze wszystkimi innymi figurami, jeśli
hetman zbije figurę przeciwnika, jego ruch jest zakończony.

![Hetman](./img/6.png "Hetman"){ height=40% width=40%}

## 11. Wieża - 5 pkt

Wieża może poruszać się tak daleko, jak chce, ale tylko do przodu,
do tyłu i na boki.

![Wieża](./img/7.png "Wieża"){ height=60% width=60%}

## 12. Goniec - 3 pkt

Goniec może iść tak daleko, jak chce, ale tylko po skosie. Każdy
goniec rozpoczyna partię na jednym kolorze (białym lub czarnym) i
musi zawsze zostać na tym kolorze.

![Goniec](./img/8.png "Goniec"){ height=60% width=60%}

## 13. Pionek - 1pkt

Pionki są niezwykłe ponieważ (w przeciwieństwie do figur) poruszają
się inaczej niż biją: chodzą prosto, natomiast biją po skosie. Pionek
porusza się o jedno pole do przodu, za wyjątkiem swojego
pierwszego ruchu w danej partii, kiedy to ma możliwość ruchu o 2
pola do przodu. Bije natomiast zawsze o jedno pole po skosie. Pionek
nigdy nie porusza się ani nie bije do tyłu. Jeśli figura przeciwnika
znajduje się na polu bezpośrednio przed pionkiem, nie może on
poruszyć się ani zbić tej figury.

![Pionek](./img/9.png "Pionek"){ height=30% width=30%}

## 14. Pozostałe zasady

\begin{alertblock}{Uwaga}
Ze względu na ograniczoną ilość treści, którą mogę umieścić w prezentacji przez liczbę commit'ów, pozostałe zasady tylko wymienię.
\end{alertblock}

-   Promocja pionka
-   En passant
-   Roszada
